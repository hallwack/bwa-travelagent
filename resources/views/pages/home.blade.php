@extends('layouts.app')

@section('title')
    travelagent.id
@endsection

@section('content')
<!-- Header -->
<header class="text-center">
  <h1>Explore The Beautiful World With Us</h1>
  <p class="mt-3">The Journey You'll Never See</p>
  <a href="#" class="btn btn-get-started px-4 py-2 mt-4">Get Started</a>
</header>

<!-- main content -->
<main>
  <!-- statistics -->
  <div class="container">
    <section class="section-stats row justify-content-center" id="stats">
      <div class="col-3 col-md-2 stats-detail">
        <h2>20K</h2>
        <p>Members</p>
      </div>
      <div class="col-3 col-md-2 stats-detail">
        <h2>20+</h2>
        <p>Countries</p>
      </div>
      <div class="col-3 col-md-2 stats-detail">
        <h2>50+</h2>
        <p>Hotels</p>
      </div>
      <div class="col-3 col-md-2 stats-detail">
        <h2>10+</h2>
        <p>Partners</p>
      </div>
    </section>
  </div>

  <!-- popular section title -->
  <section class="section-popular" id="popular">
    <div class="container">
      <div class="row">
        <div class="col text-center section-popular-heading">
          <h2>Paket Popular</h2>
          <p>Something that you never try before in this world</p>
        </div>
      </div>
    </div>
  </section>

  <!-- popular content -->
  <section class="section-popular-content" id="popularContent">
    <div class="container">
      <div class="section-popular-travel row justify-content-center">
        @foreach ($items as $item)
        <div class="col-sm-6 col-md-4 col-lg-3">
          <div
            class="card-travel text-center d-flex flex-column"
            style="background-image: url('{{ $item->galleries->count() ? Storage::url($item->galleries->first()->image) : '' }}');"
          >
            <div class="travel-country">{{ $item->location }}</div>
            <div class="travel-location">{{ $item->title }}</div>
            <div class="travel-button mt-auto">
              <a href="{{ route('detail', $item->slug) }}" class="btn btn-travel-details px-4"
                >View Details</a
              >
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>

  <!-- our networks section -->
  <section class="section-networks">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h2>Our Partners</h2>
          <p>
            We are also trusted <br />
            by big companies
          </p>
        </div>
        <div class="col-md-8 text-center">
          <img
            src="frontend/images/logo-companies.png"
            alt="logo-companies"
            class="img-partners"
          />
        </div>
      </div>
    </div>
  </section>

  <!-- testimonial section heading -->
  <section class="section-testimonial-heading" id="testimonialHeading">
    <div class="container">
      <div class="row">
        <div class="col text-center">
          <h2>They're Who Satisfied With Us</h2>
          <p>
            Moment were giving them <br />
            the best experience
          </p>
        </div>
      </div>
    </div>
  </section>

  <!-- testimonial section content -->
  <section class="section-testimonial-content" id="testimonialContent">
    <div class="container">
      <div class="section-popular-travel row justify-content-center">
        <div class="col-sm-6 col-md-6 col-lg-4">
          <div class="card card-testimonial text-center">
            <div class="testimonial-content">
              <img
                src="frontend/images/pic-testimonial.png"
                alt="pic-testimonial"
                class="mb-4 rounded-circle"
              />
              <h3 class="mb-4">Bapak Ari</h3>
              <p class="testimonial">
                "What a mind-blowing trip it was! I shall always keep the
                wonderful experiences close to my heart. With heartfelt
                thanks to Charles, the trip was very well planned and
                executed. Nancy was beyond awesome."
              </p>
              <hr />
              <p class="trip-to mt-2">Trip to Moscow, Rusia</p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-4">
          <div class="card card-testimonial text-center">
            <div class="testimonial-content">
              <img
                src="frontend/images/pic-testimonial.png"
                alt="pic-testimonial"
                class="mb-4 rounded-circle"
              />
              <h3 class="mb-4">Bapak Ari</h3>
              <p class="testimonial">
                "What a mind-blowing trip it was! I shall always keep the
                wonderful experiences close to my heart. With heartfelt
                thanks to Charles, the trip was very well planned and
                executed. Nancy was beyond awesome."
              </p>
              <hr />
              <p class="trip-to mt-2">Trip to Moscow, Rusia</p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-4">
          <div class="card card-testimonial text-center">
            <div class="testimonial-content">
              <img
                src="frontend/images/pic-testimonial.png"
                alt="pic-testimonial"
                class="mb-4 rounded-circle"
              />
              <h3 class="mb-4">Bapak Ari</h3>
              <p class="testimonial">
                "What a mind-blowing trip it was! I shall always keep the
                wonderful experiences close to my heart. With heartfelt
                thanks to Charles, the trip was very well planned and
                executed. Nancy was beyond awesome."
              </p>
              <hr />
              <p class="trip-to mt-2">Trip to Moscow, Rusia</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-center">
          <a href="#" class="btn btn-need-help mt-4 mx-1 px-4"
            >I Need Help</a
          >
          <a href="{{ route('register') }}" class="btn btn-get-started mt-4 mx-1 px-4"
            >Get Started</a
          >
        </div>
      </div>
    </div>
  </section>
</main>
@endsection