@extends('layouts.checkout')
@section('title', 'Checkout')

@section('content')
<main>
  <section class="section-details-header"></section>
  <section class="section-details-content">
    <div class="container">
      <div class="row">
        <div class="col p-0">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item">Paket Travel</li>
              <li class="breadcrumb-item">Item</li>
              <li class="breadcrumb-item active">Checkout</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8 pl-lg-0">
          <div class="card card-details">
            @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
            @endif
            <h1>Who's Going</h1>
            <p>Trip to {{ $item->travel_packages->title }}, {{ $item->travel_packages->location }}</p>
            <div class="attendee">
              <table class="table table-responsive-sm text-center">
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Nasionality</td>
                    <td>VISA</td>
                    <td>Passport</td>
                    <td></td>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($item->details as $detail)
                    <tr>
                      <td class="align-middle">{{ $detail->username }}</td>
                      <td class="align-middle">{{ $detail->nationality }}</td>
                      <td class="align-middle">{{ $detail->is_visa ? '30 Days' : 'N/A'}}</td>
                      <td class="align-middle">{{ \Carbon\Carbon::createFromDate($detail->doe_passport) > \Carbon\Carbon::now() ? 'Active' : 'Inactive' }}</td>
                      <td class="align-middle">
                        <a href="{{ route('checkout-remove', $detail->id) }}">
                          <img
                            src="{{ url('frontend/images/ic_close.png') }}"
                            alt="ic_close"
                          />
                        </a>
                      </td>
                    </tr>
                  @empty
                    <tr>
                      <td colspan="6" class="text-center">No Visitor</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
            </div>

            <div class="member mt-3">
              <h2>Add Member</h2>
              <form class="form-inline" method="POST" action="{{ route('checkout-create', $item->id) }}">
                @csrf
                <label for="username" class="sr-only">Name</label>
                <input
                  type="text"
                  name="username"
                  class="form-control mb-2 mr-sm-2 col-lg-3 col-0"
                  placeholder="Username"
                  requireddoe_passport
                  id="username"
                />
                <label for="nationality" class="sr-only">Name</label>
                <input
                  type="text"
                  name="nationality"
                  class="form-control mb-2 mr-sm-2 col-lg-4 col-0"
                  placeholder="Nationality"
                  required
                  id="nationality"
                />
                <label for="is_visa" class="sr-only">VISA</label>
                <select
                  name="is_visa"
                  id="is_visa"
                  required
                  class="custom-select mb-2 mr-sm-2"
                >
                  <option value="VISA">VISA</option>
                  <option value="1">30 Days</option>
                  <option value="0">N/A</option>
                </select>
                <label for="doe_passport" class="sr-only"
                  >DOE Passport</label
                >
                <div class="input-group mb-2 mr-sm-2 col-0 col-lg-4 px-0">
                  <input
                    type="text"
                    id="doePassport"
                    name="doe_passport"
                    class="form-control datepicker"
                    placeholder="DOE Passport"
                  />
                </div>

                <button
                  class="btn btn-add-now mb-2 px-3 ml-xl-5"
                  type="submit"
                >
                  Add Now
                </button>
                <div class="container">
                  <div class="row">
                    <div class="disclaimer col py-2 my-2">
                      <h3 class="mt-2 mb-0">Note</h3>
                      <p class="mb-0">
                        You are only able to invite member that has
                        registered in TravelAgent
                      </p>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card card-details card-right">
            <h2>Checkout Informations</h2>
            <table class="trip-informations">
              <tr>
                <th widht="50%">Members</th>
                <td width="50%" class="text-right">{{ $item->details->count() }} Persons</td>
              </tr>
              <tr>
                <th widht="50%">Additional VISA</th>
                <td width="50%" class="text-right">${{ $item->additional_visa }}.00</td>
              </tr>
              <tr>
                <th widht="50%">Trip Price</th>
                <td width="50%" class="text-right">${{ $item->travel_packages->price }}.00/Person</td>
              </tr>
              <tr>
                <th widht="50%">Total Price</th>
                <td width="50%" class="text-right">${{ $item->transaction_total }}.00</td>
              </tr>
              <tr>
                <th widht="50%">Total Price (+Unique Code)</th>
                <td width="50%" class="text-right text-total">
                  <span class="text-blue">${{ $item->transaction_total }},</span
                  ><span class="text-orange">{{ mt_rand(0,99) }}</span>
                </td>
              </tr>
            </table>
            <hr />
            <h2>Payment Instruction</h2>
            <p class="payment-instruction">
              Please complete your payment <br />
              before you continue this trip
            </p>
            <div class="bank">
              <div class="bank-item pb-3">
                <img
                  src="{{ url('frontend/images/ic_bank_blue.png') }}"
                  alt=""
                  class="bank-image"
                />
                <div class="description">
                  <h3>PT. TRAVELIA INDONESIA</h3>
                  <p>
                    Bank Mandiri <br />
                    1129 1273 1232
                  </p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="bank-item pb-3">
                <img
                  src="{{ url('frontend/images/ic_bank_red.png') }}"
                  alt=""
                  class="bank-image"
                />
                <div class="description">
                  <h3>PT. TRAVELIA INDONESIA</h3>
                  <p>
                    Bank Century <br />
                    1129 1273 1232
                  </p>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="join-container">
            <a
              href="{{ route('checkout-success', $item->id) }}"
              class="btn btn-block btn-join-now mt-3 py-2"
              >I Have Made Payment</a
            >
          </div>
          <div class="text-center mt-3">
            <a href="{{ route('detail', $item->travel_packages->slug) }}" class="text-muted">CANCEL BOOKING</a>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('frontend/libraries/gijgo/gijgo.min.css') }}" />
@endpush

@push('addon-script')
<script src="{{ url('frontend/libraries/gijgo/gijgo.min.js') }}"></script>
<script>
  $(document).ready(function () {
    $(".datepicker").datepicker({
      format: 'yyyy-mm-dd',
      uiLibrary: "bootstrap4",
      icons: {
        rightIcon: '<img src="{{ url('frontend/images/ic_dropdown.png') }}" />',
      },
    });
  });
</script>
@endpush