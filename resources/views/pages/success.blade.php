@extends('layouts.success')
@section('title', 'Success')

@section('content')
<main>
  <div class="section-success d-flex align-items-center">
    <div class="col text-center">
      <img src="{{ url('frontend/images/ic_success.png') }}" />
      <h1>Yeay Success!</h1>
      <p>
        We've sent you e-mail <br />
        For trip instruction please read it as well
      </p>
      <a href="{{ route('home') }}" class="btn- btn-home-page mt-3 px-4 py-2">Home Page</a>
    </div>
  </div>
</main>
@endsection