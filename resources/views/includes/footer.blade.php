<!-- footer -->
<footer class="section-footer mt-4 mb-3 border-top">
  <div class="container pt-5 pb-5">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-2">
        <h5>Social Media</h5>
        <ul class="list-unstyled">
          <li><a href="#">Facebook</a></li>
          <li><a href="#">Twitter</a></li>
          <li><a href="#">Instagram</a></li>
        </ul>
      </div>
      <div class="col-12 col-lg-2">
        <h5>Perusahaan</h5>
        <ul class="list-unstyled">
          <li><a href="#">Tentang Kami</a></li>
          <li><a href="#">Berita</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Promosi</a></li>
        </ul>
      </div>
      <div class="col-12 col-lg-2">
        <h5>Bantuan</h5>
        <ul class="list-unstyled">
          <li><a href="#">Cara Pembayaran</a></li>
          <li><a href="#">FAQ</a></li>
          <li><a href="#">Syarat dan Ketentuan</a></li>
        </ul>
      </div>
      <div class="col-12 col-lg-6">
        <form>
          <div class="form-subscribe form-group py-2 px-3">
            <div class="form-subscribe-content mx-3 my-4">
              <div class="form-text-subscribe">
                <p class="text-subscribe">
                  Ingin mendapatkan harga paket travel termurah? <br />
                  Dapatkan info promo dan penawaran eksklusif dari kami
                </p>
              </div>
              <div class="form-register-subscribe form-inline">
                <input
                  type="email"
                  class="form-control form-email col-8 mt-2 mt-md-0"
                  placeholder="name@example.com"
                />
                <button
                  class="btn btn-subscribe ml-auto px-4 mt-2 mt-md-0"
                  type="submit"
                >
                  Submit
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <div
      class="row border-top justify-content-center align-items-center pt-3"
    >
      <div class="col-auto font-weight-500 text-gray-300">
        2020 Copyright TravelAgent.id | Made in Jakarta
      </div>
    </div>
  </div>
</footer>